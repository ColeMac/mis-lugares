//
//  Place.swift
//  Lugares
//
//  Created by Moisés Córdova on 11/11/17.
//  Copyright © 2017 Moisés Córdova. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class Place : NSManagedObject {
    @NSManaged var name : String
    @NSManaged var type : String
    @NSManaged var location : String
    @NSManaged var telephone : String?
    @NSManaged var webSite : String?
    @NSManaged var image : NSData?
    @NSManaged var rating : String?
    
}
