//
//  ViewController.swift
//  Lugares
//
//  Created by Moisés Córdova on 11/11/17.
//  Copyright © 2017 Moisés Córdova. All rights reserved.
//

import UIKit
import LocalAuthentication
import CoreData

class ViewController: UITableViewController {
    
    var places : [Place] = []
    var searchResults : [Place] = []
    var fetchResultsController : NSFetchedResultsController<Place>!
    var searchController : UISearchController!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        authenticateUser()
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        self.searchController = UISearchController(searchResultsController: nil)
        self.tableView.tableHeaderView = self.searchController.searchBar
        self.searchController.searchResultsUpdater = self
        self.searchController.dimsBackgroundDuringPresentation = false
        self.searchController.searchBar.placeholder = "Buscar Lugares"
        self.searchController.searchBar.tintColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.searchController.searchBar.tintColor = UIColor.darkGray
        
        let fetchRequest : NSFetchRequest<Place> = NSFetchRequest(entityName: "Place")
        let sortDescriptor = NSSortDescriptor(key: "name", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        
        if let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
            let context = container.viewContext
            self.fetchResultsController = NSFetchedResultsController(fetchRequest: fetchRequest, managedObjectContext: context, sectionNameKeyPath: nil, cacheName: nil)
            self.fetchResultsController.delegate = self
            do {
                try fetchResultsController.performFetch()
                self.places = fetchResultsController.fetchedObjects!
                
                if self.places.count < 8 {
                    self.loadDefaultData()
                }
            } catch {
                print("Error \(error)")
            }
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.hidesBarsOnSwipe = true
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    //MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if self.searchController.isActive {
            return self.searchResults.count
        } else {
            return self.places.count
        }
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let place : Place!
        
        if self.searchController.isActive {
            place = self.searchResults[indexPath.row]
        } else {
            place = self.places[indexPath.row]
        }
        
        let cellID = "PlaceCell"
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellID, for: indexPath) as! PlaceCell
        
        cell.thumbnailImageView.image = UIImage(data: place.image! as Data)
        cell.nameLabel.text = place.name
        cell.timeLabel.text = place.type
        cell.ingredientsLabel.text = "Dirección: \(place.location)"
        
        return cell
    }
    
    func authenticateUser() {
        let context = LAContext()
        var error: NSError?
        
        if context.canEvaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, error: &error) {
            let reason = "Identifiación"
            
            context.evaluatePolicy(.deviceOwnerAuthenticationWithBiometrics, localizedReason: reason) {
                [unowned self] success, authenticationError in
                
                DispatchQueue.main.async {
                    if success {
                        let sucessAction = UIAlertController(title: "Bienvenido", message: "Ha sido verificado", preferredStyle: .alert)
                        sucessAction.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(sucessAction, animated: true)
                    } else {
                        let ac = UIAlertController(title: "Error de autentificación", message: "No verificable", preferredStyle: .alert)
                        ac.addAction(UIAlertAction(title: "OK", style: .default))
                        self.present(ac, animated: true)
                        self.authenticateUser()
                    }
                }
            }
        } else {
            let ac = UIAlertController(title: "Touch ID no disponible", message: "Tu dispositivo no está configurado", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    
    func loadDefaultData(){
        let names = ["Estatua de la Libertad", "Basílica de la Sagrada Familia", "Big Ben", "Cristo Redentor", "Torre Eiffel", "Gran Muralla China", "Torre de Pisa", "Centro Universitario de Ciencias Exactas e Ingenierías"]
        let types = ["Monumento", "Iglesia", "Museo", "Monumento", "Monumento", "Monumento", "Monumento", "Universidad"]
        let locations = ["Liberty Island Nueva York, NY  10004 Estados Unidos", "Calle de Mallorca, 40108013 Barcelona España", "London SW1A 0AA England", "Parque Nacional da Tijuca Escadaria do Corcovado Humaitá Rio de Janeiro - RJ 21072 Brasil", "5 Avenue Anatole France 75007 Paris France", "Great Wall, Mutianyu Beijing China", "Leaning Tower of Pisa 56126 Pisa, Province of Pisa Italy", "Boulevard General Marcelino García Barragán 1421 Olímpica 44430 Guadalajara, JAL México"]
        let telephones = ["12123633200‬", "‭34932080414‬", "555321895", "55(21)2492-2252‬", "33892701239‬", "555321895", "39050835012‬", "‭523313785900‬"]
        let webs = ["https://www.nps.gov/stli/index.htm", "http://www.sagradafamilia.org", "http://parliament.uk/about/living-heritage/building/palace/big-ben", "http://cristoredentoroficial.com.br", "http://www.toureiffel.paris", "http://www.mutianyugreatwall.com", "http://opapisa.it", "http://www.cucei.udg.mx"]
        let images = [#imageLiteral(resourceName: "Estatua de la Libertad"),#imageLiteral(resourceName: "Sagrada Familia"),#imageLiteral(resourceName: "bigben"),#imageLiteral(resourceName: "cristoredentor"),#imageLiteral(resourceName: "torreeiffel"),#imageLiteral(resourceName: "murallachina"),#imageLiteral(resourceName: "torrepisa"),#imageLiteral(resourceName: "CUCEI")]
        
        if let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
            let context = container.viewContext
            
            for i in 0..<names.count {
                
                let place = NSEntityDescription.insertNewObject(forEntityName: "Place", into: context) as? Place
                
                place?.name = names[i]
                place?.type = types[i]
                place?.location = locations[i]
                place?.telephone = telephones[i]
                place?.webSite = webs[i]
                place?.rating = "rating"
                place?.image = (UIImagePNGRepresentation(images[i])! as NSData)
                
                do {
                    try context.save()
                } catch {
                    print("Ha habido un error al guardar el lugar en Core Data")
                }
            }
        }
    }
    
    override func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        //Compartir
        let shareAction = UITableViewRowAction(style: .default, title: "Compartir") { (action, indexPath) in
            
            let place : Place!
            
            if self.searchController.isActive {
                place = self.searchResults[indexPath.row]
            } else {
                place = self.places[indexPath.row]
            }
            
            let shareDefaultText = "Estoy visitando \(place.name)"
            
            let activityController = UIActivityViewController(activityItems: [shareDefaultText, UIImage(data: place.image! as Data)!], applicationActivities: nil)
            self.present(activityController, animated: true, completion: nil)
        }
        
        shareAction.backgroundColor = UIColor(red: 30.0/255.0, green: 164.0/255.0, blue: 253.0/255.0, alpha: 1.0)
        
        //Borrar
        let deleteAction = UITableViewRowAction(style: .default, title: "Borrar") { (action, indexPath) in
            if let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
                let context = container.viewContext
                let placeToDelete = self.fetchResultsController.object(at: indexPath)
                context.delete(placeToDelete)
                
                do {
                    try context.save()
                } catch {
                    print("Error: \(error)")
                }
            }
        }
        
        deleteAction.backgroundColor = UIColor(red: 202.0/255.0, green: 202.0/255.0, blue: 202.0/255.0, alpha: 1.0)
        
        return [shareAction, deleteAction]
    }
    
    //MARK: - UITableViewDelegate
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "showDetail" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let place : Place!
                if self.searchController.isActive {
                    place = self.searchResults[indexPath.row]
                } else {
                    place = self.places[indexPath.row]
                }
                let destinationViewController = segue.destination as! DetailViewController
                destinationViewController.place = place
            }
        }
    }
    
    @IBAction func unwindToMainViewController(segue: UIStoryboardSegue) {
        if segue.identifier == "unwindToMainViewController" {
            if let addPlaceVC = segue.source as? AddPlaceViewController {
                if let newPlace = addPlaceVC.place {
                    self.places.append(newPlace)
                }
            }
        }
    }
    
    func filtereContentFor(textToSearch: String) {
        self.searchResults = self.places.filter({ (place) -> Bool in
            let nameToFind = place.name.range(of: textToSearch, options: NSString.CompareOptions.caseInsensitive)
            return nameToFind != nil
        })
    }
}

extension ViewController : NSFetchedResultsControllerDelegate {
    
    func controllerWillChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.beginUpdates()
    }
    
    func controller(_ controller: NSFetchedResultsController<NSFetchRequestResult>, didChange anObject: Any, at indexPath: IndexPath?, for type: NSFetchedResultsChangeType, newIndexPath: IndexPath?) {
        switch type {
        case .insert:
            if let newIndexPath = newIndexPath {
                self.tableView.insertRows(at: [newIndexPath], with: .fade)
            }
        case .delete:
            if let indexPath = indexPath {
                self.tableView.deleteRows(at: [indexPath], with: .fade)
            }
        case .update:
            if let indexPath = indexPath {
                self.tableView.reloadRows(at: [indexPath], with: .fade)
            }
        case .move:
            if let indexPath = indexPath, let newIndexPath = newIndexPath {
                self.tableView.moveRow(at: indexPath, to: newIndexPath)
            }
        }
        self.places = controller.fetchedObjects as! [Place]
    }
    
    func controllerDidChangeContent(_ controller: NSFetchedResultsController<NSFetchRequestResult>) {
        self.tableView.endUpdates()
    }
}

extension ViewController : UISearchResultsUpdating {
    func updateSearchResults(for searchController: UISearchController) {
        if let searchText = searchController.searchBar.text {
            self.filtereContentFor(textToSearch: searchText)
            self.tableView.reloadData()
        }
    }
}
