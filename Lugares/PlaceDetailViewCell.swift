//
//  PlaceDetailViewCell.swift
//  Lugares
//
//  Created by Moisés Córdova on 11/11/17.
//  Copyright © 2017 Moisés Córdova. All rights reserved.
//

import UIKit

class PlaceDetailViewCell: UITableViewCell {
    
    @IBOutlet var keyLabel: UILabel!
    
    @IBOutlet var valueLabel: UILabel!
}
