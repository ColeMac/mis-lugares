//
//  PlaceCell.swift
//  Lugares
//
//  Created by Moisés Córdova on 11/11/17.
//  Copyright © 2017 Moisés Córdova. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {
    @IBOutlet var thumbnailImageView: UIImageView!
    @IBOutlet var nameLabel: UILabel!
    @IBOutlet var timeLabel: UILabel!
    @IBOutlet var ingredientsLabel: UILabel!
    
}
