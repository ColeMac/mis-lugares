//
//  AddPlaceViewController.swift
//  Lugares
//
//  Created by Moisés Córdova on 11/11/17.
//  Copyright © 2017 Moisés Córdova. All rights reserved.
//

import UIKit
import CoreData

class AddPlaceViewController: UITableViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextFiled: UITextField!
    @IBOutlet weak var typeTextField: UITextField!
    @IBOutlet weak var directionTextField: UITextField!
    @IBOutlet weak var telephoneTextField: UITextField!
    @IBOutlet weak var webTextField: UITextField!
    @IBOutlet weak var dontLikeButton: UIButton!
    @IBOutlet weak var likeButton: UIButton!
    @IBOutlet weak var loveButton: UIButton!
    @IBOutlet weak var imageView: UIImageView!
    
    var rating : String?
    var place : Place?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.nameTextFiled.delegate = self
        self.typeTextField.delegate = self
        self.directionTextField.delegate = self
        self.telephoneTextField.delegate = self
        self.webTextField.delegate = self
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func savePressed(_ sender: UIBarButtonItem) {
        
        if let name = self.nameTextFiled.text,
            let type = self.typeTextField.text,
            let direction = self.directionTextField.text,
            let telephone = self.telephoneTextField.text,
            let webSite = self.webTextField.text,
            let theImage = self.imageView.image,
            let rating = self.rating {
            
            if let container = (UIApplication.shared.delegate as? AppDelegate)?.persistentContainer {
                let contex = container.viewContext
                
                self.place = NSEntityDescription.insertNewObject(forEntityName: "Place", into: contex) as? Place
                
                self.place?.name = name
                self.place?.type = type
                self.place?.location = direction
                self.place?.telephone = telephone
                self.place?.webSite = webSite
                self.place?.rating = rating
                self.place?.image = (UIImagePNGRepresentation(theImage)! as NSData)
                
                do {
                    try contex.save()
                } catch {
                    print("Ha habido un error al guardar el lugar en Core Data")
                }
            }
            
            self.performSegue(withIdentifier: "unwindToMainViewController", sender: self)
            
        } else {
            let alertController = UIAlertController(title: "Falta algún dato", message: "Revisa que lo tengas todo configurado", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
        
    }
    
    @IBAction func ratingPressed(_ sender: UIButton) {
        switch sender.tag {
        case 1:
            self.rating = "dislike"
        case 2:
            self.rating = "good"
        case 3:
            self.rating = "great"
        default:
            break
        }
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.row == 0 {
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                let imagePicker = UIImagePickerController()
                imagePicker.allowsEditing = false
                imagePicker.sourceType = .photoLibrary
                imagePicker.delegate = self
                
                self.present(imagePicker, animated: true, completion: nil)
            }
        }
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        self.imageView.image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.imageView.contentMode = .scaleAspectFill
        self.imageView.clipsToBounds = true
        
        let leadingConstraint = NSLayoutConstraint(item: self.imageView, attribute: .leading, relatedBy: .equal, toItem: self.imageView.superview, attribute: .leading, multiplier: 1, constant: 0)
        leadingConstraint.isActive = true
        
        let trailingConstraint = NSLayoutConstraint(item: self.imageView, attribute: .trailing, relatedBy: .equal, toItem: self.imageView.superview, attribute: .trailing, multiplier: 1, constant: 0)
        trailingConstraint.isActive = true
        
        let topConstraint = NSLayoutConstraint(item: self.imageView, attribute: .top, relatedBy: .equal, toItem: self.imageView.superview, attribute: .top, multiplier: 1, constant: 0)
        topConstraint.isActive = true
        
        let bottonConstraint = NSLayoutConstraint(item: self.imageView, attribute: .bottom, relatedBy: .equal, toItem: self.imageView.superview, attribute: .bottom, multiplier: 1, constant: 0)
        bottonConstraint.isActive = true
        
        dismiss(animated: true, completion: nil)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
}
